#include <poppler-qt5.h>
#include <QFont>
#include <QDebug>

/*
 * Create what I think is a typewriter annotation using poppler QT5 frontend.
 *
 * Note:
 * Okular doesn't create annotations exactly this way.
 * Instead it creates a QDomElement tree where it fills in all required meta data,
 * and then passes the QDomElement to AnnotationUtils::createAnnotation().
 * But the result is the same.
 */
Poppler::TextAnnotation* createTypewriter() {
    Poppler::TextAnnotation *annot = new Poppler::TextAnnotation( Poppler::TextAnnotation::InPlace );
    annot->setFlags(0);
    annot->setBoundary( QRectF(0.1, 0.3, 0.15, 0.025) ); // rect given normalized to page geometry
    annot->setAuthor( QString("Tobias Deiminger") );
    annot->setContents( QString("Lorem ipsum") ); // This text appears in open and closed state.
    annot->setTextFont( QFont( "Helvetica", 12 ) );
    annot->setInplaceAlign( 0 );  // quaddingLeftJustified = 0, quaddingCentered = 1, quaddingRightJustified = 2
    annot->setInplaceIntent(Poppler::TextAnnotation::TypeWriter);
    Poppler::Annotation::Style s;
    s.setColor( QColor(255,255,255,0) ); // Color of background. Last value is alpha, 0 means transparent background.
    s.setWidth( 0 ); // border width
    s.setOpacity( 1 ); // Opacity. Similar to setColor, affects whole appearance=border+background+font.
    annot->setStyle( s );
    return annot;
}

/*
 * This example shows how to create and save a typewriter annotation with poppler.
 * It's PDF specific, as PDF is arguably the most important use case.
 * But keep in mind Okular supports annotations for all file types, not only PDF.
 * Okular has fallback rendering in PagePainter::paintCroppedPageOnPainter to do so.
 * To persist annotations where they're not supported natively,
 * Okular can save annotation data besides the document in XML files.
 */
int main(int argc, char *argv[]) {
    Poppler::Document* document = Poppler::Document::load("example.pdf");

    /* Load document and get first page. */
    if (!document || document->isLocked()) {
      qDebug() << "Can't open document";
      delete document;
    }
    Poppler::Page* pdfPage = document->page(0);
    if (pdfPage == 0) {
      qDebug() << "Can't open page";
    }

    /* Create and add annotation to the in-memory document. */
    Poppler::TextAnnotation* typewriter_annot = createTypewriter();
    pdfPage->addAnnotation(typewriter_annot);

    /* Save the in-memory document to file. */
    Poppler::PDFConverter *pdfConv = document->pdfConverter();
    pdfConv->setOutputFileName( "/tmp/saved_doc.pdf" );
    pdfConv->setPDFOptions( pdfConv->pdfOptions() | Poppler::PDFConverter::WithChanges );
    bool success = pdfConv->convert();

    /* Cleanup */
    delete typewriter_annot;
    delete pdfPage;
    delete document;

    return 0;
}

