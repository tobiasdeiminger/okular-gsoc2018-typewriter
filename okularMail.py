import requests
from bs4 import BeautifulSoup
keywords = ['typewriter', 'freetext', 'render', 'paint', 'annot']
months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
years = [2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018]

for y in range(0,13):
    for m in range(0,12):
        url = 'https://mail.kde.org/pipermail/okular-devel/' + str(years[y]) + '-' + months[m] + '/'
        url1 = url + 'subject.html'
        r = requests.get(url1)
        if r.status_code != requests.codes.ok:
            continue
        soup = BeautifulSoup(r.text, 'html.parser')
        ultag = soup.find_all('ul')[1]
        for i in ultag.find_all('li'):
            if i.find('li'):
                for word in keywords:
                    if word in i.li.a.text.strip().lower():
                        print(i.li.a.text.strip(), '---', url + i.li.a['href'].strip())
